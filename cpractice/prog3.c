#include <stdio.h> 
 
struct EmployeeType 
{   
    int id_number;     
    int age;   
    float salary; 
}; 
 
int main() { 
  struct EmployeeType  emp1, emp2;    
 
  emp1.age = 22;   
  emp1.id_number = 1;  
  emp1.salary = 12000.0;  
  emp2.age = 45; 
  
  struct EmployeeType  emp3; 
  struct EmployeeType  *emp_ptr; 
 
  emp_ptr= &emp3;      
  emp_ptr-> age = 25;     
  printf( "Employee 1 id number = %d\n", emp1.id_number);
  printf( "Employee 1 salary = %4f\n", emp1.salary);          
  printf( "Employee 2 age = %d\n", emp2.age);
  printf( "Employee 3 age = %d\n", emp3.age);
  return 0; 

} 

